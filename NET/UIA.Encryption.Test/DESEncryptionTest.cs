﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UIA.Encryption
{
    [NUnit.Framework.TestFixture]
    public class DESEncryptionTest
    {
        [NUnit.Framework.Test]
        public void TestEncryptHello()
        {
            DESEncryption des1 = new DESEncryption();
            Console.WriteLine("ECB: " + des1.Encrypt("oNBoNX123456XOnBOn"));
            DESEncryption des2 = new DESEncryption(false);
            Console.WriteLine("CBC: " + des2.Encrypt("oNBoNX123456XOnBOn"));
        }

        [NUnit.Framework.Test]
        public void TestEncryptToString()
        {
            string stringToEncrypt = "hello 這是 DES 加解密測試。";

            DESEncryption des = new DESEncryption();
            string encrypted = des.Encrypt(stringToEncrypt);
            string decrypted = des.DecryptToString(encrypted);

            Console.WriteLine(stringToEncrypt);
            Console.WriteLine(encrypted);
            Console.WriteLine(decrypted);
        }
    }
}
