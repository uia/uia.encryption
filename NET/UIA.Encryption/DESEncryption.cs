﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace UIA.Encryption
{
    public class DESEncryption
    {

        private DESCryptoServiceProvider des;

        public DESEncryption(bool ecb = true)
            : this(Encoding.UTF8.GetBytes("pISdesEn"), ecb)
        {
        }

        public DESEncryption(string keySpecName, bool ecb = true)
            : this(Encoding.UTF8.GetBytes(keySpecName), ecb)
        {
        }

        public DESEncryption(byte[] keySpecName, bool ecb = true)
        {
            this.des = new DESCryptoServiceProvider();
            this.des.Key = keySpecName;
            if (ecb)
            {
                this.des.Mode = CipherMode.ECB;
            }
            else
            {
                this.des.IV = keySpecName;
            }
        }

        public string Encrypt(string unencryptedString)
        {
            return Encrypt(Encoding.UTF8.GetBytes(unencryptedString));
        }

        public string Encrypt(byte[] unencryptedBytes)
        {
            string encryptedString = null;
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, this.des.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(unencryptedBytes, 0, unencryptedBytes.Length);
                    cs.FlushFinalBlock();
                    encryptedString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptedString;
        }

        public string DecryptToString(string encryptedString)
        {
            return Encoding.UTF8.GetString(DecryptToBytes(encryptedString));
        }

        public byte[] DecryptToBytes(string encryptedString)
        {
            byte[] unencryptedBytes = Convert.FromBase64String(encryptedString);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, this.des.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(unencryptedBytes, 0, unencryptedBytes.Length);
                    cs.FlushFinalBlock();
                    return ms.ToArray();
                }
            }
        }
    }
}
