package uia;

import org.junit.Test;

import uia.data.MD5;

public class MD5Test {

	@Test
	public void testBuild() throws Exception {
		String file = MD5Test.class.getResource("sample.zip").getFile();
		System.out.println("build :" + MD5.build(file));
	}

	@Test
	public void testReadFile() throws Exception {
		String md5File = MD5Test.class.getResource("sample.md5").getFile();
		System.out.println("read  :" + MD5.readFile(md5File));
	}

	@Test
	public void testValidate() throws Exception {
		String file = MD5Test.class.getResource("sample.zip").getFile();
		String md5File = MD5Test.class.getResource("sample.md5").getFile();
		System.out.println("validate:" + MD5.validate(file, md5File));
	}
}
