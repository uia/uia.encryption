package uia.data;

import org.junit.Test;

import uia.data.DESEncryption;

public class DESEncryptionTest {

	@Test
	public void testEncryptHello() throws Exception {
        DESEncryption des1 = new DESEncryption();
        System.out.println("ECB: "+ des1.encrypt("Hello"));
        DESEncryption des2 = new DESEncryption(false);
        System.out.println("CBC: "+ des2.encrypt("Hello"));
    }

	public void testEncrypt() throws Exception {
        String stringToEncrypt = "hello 這是 DES 加解密測試。";

        DESEncryption des1 = new DESEncryption();
        System.out.println(des1.encrypt(stringToEncrypt));
 
        DESEncryption des2 = new DESEncryption("ABCDEFGH", true);
        System.out.println(des2.encrypt(stringToEncrypt));
    }

	public void testEncryptString() throws Exception {
        String stringToEncrypt = "hello 這是 DES 加解密測試。";

        DESEncryption des = new DESEncryption();
        String encrypted = des.encrypt(stringToEncrypt);
        String decrypted = des.decryptToString(encrypted);
 
        System.out.println(stringToEncrypt);
        System.out.println(encrypted);
        System.out.println(decrypted);
    }

	public void testEncryptBytes() throws Exception {
		byte[] bytesToEncrypt = new byte[] {
		    (byte)0x01,
		    (byte)0x03,
		    (byte)0x5a,
		    (byte)0xfc,
		    (byte)0x3b,
		    (byte)0x4c,
		    (byte)0xc9
		};
		
		DESEncryption des = new DESEncryption();
		 
        String encrypted = des.encrypt(bytesToEncrypt);
        byte[] decrypted = des.decryptToBytes(encrypted);
 
        print(bytesToEncrypt);
        System.out.println(encrypted);
        print(decrypted);
    }
	
	private void print(byte[] data) {
		for(int i=0; i<data.length; i++) {
	        System.out.print(String.format("%02x ", data[i]));
		}
		System.out.println();
	}
}
