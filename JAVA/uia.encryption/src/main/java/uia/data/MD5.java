package uia.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;

public class MD5 {
	
	public static String readFile(String md5File) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(md5File));	
		try {
	        return br.readLine();
	    } finally {
			br.close();
	    }
	}
	
	public static String build(String file) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		return DigestUtils.md5Hex(fis);
	}

	public static boolean validate(String file, String md5File) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		String md5 = DigestUtils.md5Hex(fis);
		return md5.equals(readFile(md5File));
	}
}
