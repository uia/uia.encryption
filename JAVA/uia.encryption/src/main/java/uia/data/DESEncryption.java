package uia.data;
 
import java.security.spec.KeySpec;
 
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
 
@SuppressWarnings("restriction")
public class DESEncryption {
 
    public static final String DES_ENCRYPTION_SCHEME = "DES";

    public static final String TRANSFORMATION_ECB = "DES";

    public static final String TRANSFORMATION_CBC = "DES/CBC/PKCS5Padding";

    private static final String UNICODE_FORMAT = "UTF8";
    
    private boolean ecb;
    
    private Cipher cipher;
    
    private SecretKey secretKey;

	private IvParameterSpec iv;
    
    public DESEncryption() throws Exception
    {
    	this(true);
    }
 
    public DESEncryption(boolean ecb) throws Exception
    {
    	this("pISdesEn".getBytes(), ecb);
    }
    
    public DESEncryption(final String keySpecName, boolean ecb) throws Exception {
    	this(keySpecName.getBytes(UNICODE_FORMAT), ecb);
    }
    
    public DESEncryption(final byte[] keySpecName, boolean ecb) throws Exception {
    	if(keySpecName.length < 8) {
    		throw new IllegalArgumentException("The length of KeySpecName must be greater than or equal to 8.");
    	}
    	KeySpec keySpec = new DESKeySpec(keySpecName);
    	SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES_ENCRYPTION_SCHEME);
    	if(ecb) {
    		this.cipher = Cipher.getInstance(TRANSFORMATION_ECB);
    	}
    	else {
    		this.cipher = Cipher.getInstance(TRANSFORMATION_CBC);
    	}
        this.secretKey = keyFactory.generateSecret(keySpec);
        this.iv = new IvParameterSpec(keySpecName);
        this.ecb = ecb;
    }

    /**
     * Encrypt the string.
     * 
     * @param unencryptedString
     * @return
     */
    public String encrypt(byte[] unencryptedBytes) {
        String encryptedString = null;
        try {
        	if(this.ecb) {
        		this.cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
        	}
        	else {
        		this.cipher.init(Cipher.ENCRYPT_MODE, this.secretKey, this.iv);
        	}
            byte[] encryptedText = this.cipher.doFinal(unencryptedBytes);
            BASE64Encoder base64encoder = new BASE64Encoder();
            encryptedString = base64encoder.encode(encryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedString;
    }

    /**
     * Encrypt the string.
     * 
     * @param unencryptedString
     * @return
     */
    public String encrypt(String unencryptedString) {
    	try {
    		return encrypt(unencryptedString.getBytes(UNICODE_FORMAT));
    	}
    	catch(Exception ex) {
    		return null;
    	}
    }

    /**
     * Decrypt the string.
     * 
     * @param encryptedString
     * @return
     */
    public byte[] decryptToBytes(String encryptedString) {
        byte[] unencryptedBytes = null;
        try {
        	if(this.ecb) {
        		this.cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
        	}
        	else {
        		this.cipher.init(Cipher.DECRYPT_MODE, this.secretKey, this.iv);
        	}
            BASE64Decoder base64decoder = new BASE64Decoder();
            byte[] encryptedText = base64decoder.decodeBuffer(encryptedString);
            unencryptedBytes = this.cipher.doFinal(encryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unencryptedBytes;
    }

    /**
     * Decrypt the string.
     * 
     * @param encryptedString
     * @return
     */
    public String decryptToString(String encryptedString) {
    	byte[] unencryptedBytes = decryptToBytes(encryptedString);
    	if(unencryptedBytes == null) {
    		return null;
        }
    	try {
    		return new String(unencryptedBytes, UNICODE_FORMAT);
    	}
    	catch(Exception ex) {
    		return null;
    	}
    }
}